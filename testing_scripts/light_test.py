#!/usr/bin/env python3

from ev3dev.ev3 import *
from time import sleep

os.system('setfont Lat15-Terminus24x12')

cs = ColorSensor('in1') 
assert cs.connected, "Connect a color sensor to sensor port 1"

cs.mode = 'COL-REFLECT'

while True:
    print('Brightness: %i' % cs.value())
    sleep(.5)
