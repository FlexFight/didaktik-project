#!/usr/bin/env python3
from ev3dev.ev3 import *
from time import sleep

os.system('setfont Lat15-Terminus24x12')

cs = ColorSensor()
assert cs.connected, "Connect a color sensor to sensor port 1"

cs.mode = 'RGB-RAW'

while True:
    print('R: %i, G: %i, B: %i' % (cs.value(0), cs.value(1), cs.value(2)))
    sleep(0.5)
