#!/usr/bin/env python3
from ev3dev.ev3 import *

os.system('setfont Lat15-Terminus24x12')

us = UltrasonicSensor() 
assert us.connected, "Connect a single US sensor to any sensor port"

# Put the US sensor into distance mode.
us.mode = 'US-DIST-CM'

while True:
    distance = us.value()
    print(str(distance))
