#!/usr/bin/env python3
from ev3dev.ev3 import *
from time import sleep

os.system('setfont Lat15-Terminus24x12')

# Connect gyro sensor and check connected.
gy = GyroSensor()
assert gy.connected, "Connect a gyro sensor to any sensor port"

# Put the gyro sensor into GYRO-ANG mode
# to measure the turn angle in degrees
gy.mode = 'GYRO-ANG'

def mod_angel(angle):
    return angle % 360

while True:  # loop until turn angle exceeds 45 degrees
    print(mod_angel(gy.value()))
    sleep(0.5)
