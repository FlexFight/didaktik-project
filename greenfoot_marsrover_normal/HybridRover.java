import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import java.net.*;

// Changes made to Rover.java:
//      - private Display anzeige; -> protected Display anzeige;
//      - private void nachricht(String pText) -> protected void nachricht(String pText)
//      - remove Greenfoot.delay(1); (line 139)
//      - remove removeTouching(Gestein.class); (line 140)

// Vision:
//      - Add basic authentication -> generate token (e.g. current timestamp) and pass via reset
//                                 -> every request has to use token
//                                 -> token can be freed with endpoint or after timeout (no action after x sec)

// Todo:
//      - synchronize water proportion
//      - pass field dimensions and stop rover at lower and right border

public class HybridRover extends Rover {
    private boolean connected = false;
    private RoverApi roverApi;

    public void act() {
        if (!connected) {
            super.nachricht("Nicht verbunden!");
        } else {
            super.act();
        }
    }

    public void setIp(String ip_address) {
        roverApi = new RoverApi(ip_address, getX(), getY());
        connected = roverApi.getConnected();
    }

    public void fahre() {
        super.fahre();
        roverApi.drive();
    }

    public void drehe(String richtung) {
        super.drehe(richtung);
        roverApi.turn(richtung);
    }

    public boolean gesteinVorhanden() {
        boolean hasStone = super.gesteinVorhanden();
        roverApi.hasStone();
        return hasStone;
    }

    public boolean huegelVorhanden(String richtung) {
        boolean hasMountain = super.huegelVorhanden(richtung);
        return hasMountain;
    }

    public void analysiereGestein() {
        super.analysiereGestein();
        roverApi.analyzeStone();
    }

    public void setzeMarke() {
        super.setzeMarke();
        roverApi.setMark();
    }

    public boolean markeVorhanden() {
        boolean hasMark = super.markeVorhanden();
        roverApi.hasMark();
        return hasMark;
    }

    public void entferneMarke() {
        super.entferneMarke();
        roverApi.removeMark();
    }

    protected void nachricht(String pText) {
        if (this.anzeige != null) {
            this.anzeige.anzeigen(pText);
            roverApi.message(pText);
            this.anzeige.loeschen();
        }
    }

    private void displayAusschalten() {
        this.getWorld().removeObject(this.anzeige);
    }

    protected void addedToWorld(World world) {
        super.addedToWorld(world);
        super.nachricht("Bitte IP vergeben!");
    }

    private class RoverApi {
        private static final String ENDPOINT_DRIVE              = "/fahre/";
        private static final String ENDPOINT_TURN               = "/drehe/%s/";
        private static final String ENDPOINT_HAS_STONE          = "/gestein_vorhanden/";
        private static final String ENDPOINT_HAS_MOUNTAIN       = "/huegel_vorhanden/%s/";
        private static final String ENDPOINT_ANALYZE_STONE      = "/analysiere_gestein/";
        private static final String ENDPOINT_SET_MARK           = "/setze_marke/";
        private static final String ENDPOINT_HAS_MARK           = "/marke_vorhanden/";
        private static final String ENDPOINT_REMOVE_MARK        = "/entferne_marke/";
        private static final String ENDPOINT_MESSAGE            = "/nachricht/%s/";
        private static final String ENDPOINT_RESET              = "/reset/%d/%d/";

        private String ipAddress;
        private boolean connected = false;

        RoverApi(String ipAddress, int x, int y) {
            if (!(ipAddress.startsWith("http://") | ipAddress.startsWith("https://")))
                ipAddress = "http://" + ipAddress;

            this.ipAddress = ipAddress + ":8080";   // defined in webserver.py

            connect(x, y);
        }

        boolean getConnected() {
            return connected;
        }

        void drive() {
            requestUrl(ipAddress + ENDPOINT_DRIVE);
        }

        void turn(String direction) {
            requestUrl(ipAddress + String.format(ENDPOINT_TURN, direction));
        }

        boolean hasStone() {
            return requestUrl(ipAddress + ENDPOINT_HAS_STONE).get("gestein").equals("ja");
        }

        boolean hasMountain(String direction) {
            return requestUrl(ipAddress + String.format(ENDPOINT_HAS_MOUNTAIN, direction)).get("huegel").equals("ja");
        }

        void analyzeStone() {
            requestUrl(ipAddress + ENDPOINT_ANALYZE_STONE);
        }

        void setMark() {
            requestUrl(ipAddress + ENDPOINT_SET_MARK);
        }

        boolean hasMark() {
            return requestUrl(ipAddress + ENDPOINT_HAS_MARK).get("marke").equals("ja");
        }

        void removeMark() {
            requestUrl(ipAddress + ENDPOINT_REMOVE_MARK);
        }

        void message(String text) {
            requestUrl(ipAddress + String.format(ENDPOINT_MESSAGE, text));
        }

        void connect(int x, int y) {
            try {
                HashMap status = parseUnnestedJsonObjectString(getRequest(ipAddress + String.format(ENDPOINT_RESET, x, y)));

                if (status.get("status").equals("OK")) {
                    connected = true;
                    message("Verbunden!");
                }
            } catch (IOException e) {
                connected = false;
            }
        }

        HashMap<String, String> requestUrl(String url) {
            if (!connected)
                return null;

            try {
                return parseUnnestedJsonObjectString(getRequest(url));
            } catch (IOException e) {
                connected = false;
                return null;
            }
        }

        /**
         * Executes a get request to a given URL and returns the response as string.
         * @param urlToRead URL to query.
         * @return Raw response string.
         */
        String getRequest(String urlToRead) throws IOException {
            StringBuilder result = new StringBuilder();
            URL url = new URL(urlToRead);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();
        }

        /**
         * Parses an unnested JSON-Object to a Hashmap.
         * @param unnestedJsonObjectString JSON String of an unnested JSON-Object.
         * @return Key-value-pairs as HashMap.
         */
        HashMap<String, String> parseUnnestedJsonObjectString(String unnestedJsonObjectString) {
            unnestedJsonObjectString = unnestedJsonObjectString.replace(" " , ""); // remove all whitespaces
            HashMap<String, String> unnestedJsonObject = new HashMap<String, String>();

            // cut brackets and leading and trailing quotation marks
            unnestedJsonObjectString = unnestedJsonObjectString.substring(2, unnestedJsonObjectString.length() - 2);
            String[] keyValueRaw = unnestedJsonObjectString.split("\",\"");

            for (String s : keyValueRaw) {
                String[] keyValue = s.split("\":\"");

                if (keyValue.length == 2) {
                    unnestedJsonObject.put(keyValue[0], keyValue[1]);
                } else {
                    unnestedJsonObject.put(keyValue[0], "");
                }
            }

            return unnestedJsonObject;
        }
    }
}
