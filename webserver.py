#!/usr/bin/env python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import json
from rover import Rover


rover = Rover()


class EV3RequestHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        path = self.path.split('/')[1:]  # first element is empty
        global rover

        if path[0] == 'fahre':
            rover.fahre()
            resp = {'status': 'OK'}

        elif path[0] == 'drehe':
            rover.drehe(path[1])
            resp = {'status': 'OK'}

        elif path[0] == 'gestein_vorhanden':
            resp = {
                'status': 'OK',
                'gestein': 'ja' if rover.gestein_vorhanden() else 'nein'
            }

        elif path[0] == 'huegel_vorhanden':
            resp = {
                'status': 'OK',
                'huegel': 'ja' if rover.huegel_vorhanden(path[1]) else 'nein'
            }

        elif path[0] == 'analysiere_gestein':
            gestein = rover.analysiere_gestein()
            resp = {
                'status': 'OK',
                'gestein_vorhanden': 'ja' if gestein[0] else 'nein',
                'farbe': gestein[1] if gestein[1] is not None else "",
                'wassergehalt': str(gestein[2]) if gestein[2] is not None else ""
            }

        elif path[0] == 'setze_marke':
            rover.setze_marke()
            resp = {'status': 'OK'}

        elif path[0] == 'marke_vorhanden':
            resp = {
                'status': 'OK',
                'marke': 'ja' if rover.marke_vorhanden() else 'nein'
            }

        elif path[0] == 'entferne_marke':
            rover.entferne_marke()
            resp = {'status': 'OK'}

        elif path[0] == 'nachricht':
            rover.nachricht(path[1])
            resp = {'status': 'OK'}

        elif path[0] == 'reset':
            if len(path) == 3:
                rover = Rover(int(path[1]), int(path[2]))
            else:
                rover = Rover()

            resp = {'status': 'OK'}

        else:
            resp = {'status': 'Endpoint not supported!'}

        self._set_response()
        self.wfile.write(json.dumps(resp).encode('utf-8'))


def run(port=8080):
    logging.basicConfig(level=logging.INFO)
    logging.info('Starting server...')

    # Server settings
    # Choose port 8080, for port 80, which is normally used for a http server, you need root access
    server_address = ('0.0.0.0', port)
    httpd = HTTPServer(server_address, EV3RequestHandler)
    logging.info('Running server...')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    logging.info('Stopping httpd...\n')


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
